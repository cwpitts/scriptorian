FROM python:3.7
MAINTAINER Christopher Pitts

ENV APP_ROOT=/ \
    APP_CONFIG=/opt/app/config.yml \
    APP_DB_PATH=/opt/app/db/lds-scriptures-sqlite3.db \
    APP_PORT=8000 \
    APP_HOST=0.0.0.0

COPY src /opt/app
COPY requirements.txt /opt/app

WORKDIR /opt/app

RUN pip3 install -r requirements.txt

CMD python3 main.py
