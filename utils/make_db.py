#!/usr/bin/env python3

import logging
import re
import sqlite3 as sql

import requests as req

verse_info_regex = re.compile("[0-9A-Za-z ]+ [0-9]+:[0-9]+") 
logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def parse_verse(verse_info):
    """ Parse verse information from a string

    Parameters
    ----------
    verse_info : str
      Verse information string

    Returns
    -------
    book : str
      Book where verse is located

    chapter : int
      Chapter number

    verse : int
      Verse number
    """
    book = verse_info[:verse_info.rfind(" ")]
    chapter, verse = map(int, verse_info[verse_info.rfind(" ") + 1:].split(":"))

    return book, chapter, verse

def parse_verses(text):
    cur_line = 0
    while not re.match(verse_info_regex, text[cur_line]):
        cur_line += 1

    print(text[cur_line])

if __name__ == "__main__":
    url = "https://www.gutenberg.org/cache/epub/17/pg17.txt"

    logger.info("Downloading text from %s", url)

    r = req.get(url)

    if r.status_code != 200:
        logger.error("Failed to download text, status was {r.status_code}")
        exit(1)

    logger.info("Downloaded text")

    with open("bom.txt", "w") as of:
        of.write(r.text)

    logger.info("Saved text")

    text = r.text.split("\r\n")

    for line in text:
        verse_info = re.match(verse_info_regex, line)
        if verse_info:
            book, chapter, verse = parse_verse(verse_info.group())
            print(book, chapter, verse)
