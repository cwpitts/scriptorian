#!/usr/bin/env python3

import os

import cherrypy

from server import Server

if __name__ == "__main__":
    app_port = int(os.environ.get("APP_PORT", 8000))
    app_root = os.environ.get("APP_ROOT", "/")
    app_host = os.environ.get("APP_HOST", "0.0.0.0")

    cherrypy.config.update({
        "server.socket_host": app_host,
        "server.socket_port": app_port,
        "log.access_file": "access.log",
        "log.error_file": "error.log"
    })

    app = cherrypy.tree.mount(Server(), app_root, {
        "/static": {
            "tools.staticdir.on": True,
            "tools.staticdir.dir": os.path.join(os.getcwd(), "static")
        }
    })

    cherrypy.engine.start()
    cherrypy.engine.block()
