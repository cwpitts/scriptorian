import os

import yaml
from jinja2 import Environment, FileSystemLoader

import cherrypy

from .api import ScriptureAPI


class Server:
    """ Main server class
    """
    def __init__(self):
        # Database
        self._db_path = os.environ["APP_DB_PATH"]
        self.api = ScriptureAPI(self._db_path)

        cherrypy.log(f"Initialized database")
        
        # Environment for rendering templates
        self._tmpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "templates")
        self._maintiner_email = os.environ["APP_EMAIL"]
        self._env = Environment(loader=FileSystemLoader(self._tmpl_dir))

        cherrypy.log("Initialized template rendering")

    def _render_template(self, tmpl_name, params={}):
        """ Render template

        Parameters
        ----------
        tmpl_name : str
          Name fo the template to render

        params : dict, optional
          Parameters to use in rendering the template, defaults to empty dict

        Returns
        -------
        str
          Rendered template
        """
        tmpl = self._env.get_template(tmpl_name)

        return tmpl.render(**params)

    @cherrypy.expose
    def index(self):
        return self._render_template("index.html", {
            "maintainer": self._maintiner_email
        })
