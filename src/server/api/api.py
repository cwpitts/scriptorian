import json
import os
import re

import cherrypy
import yaml
from jinja2 import Environment, FileSystemLoader
from sqlalchemy import and_, or_

from .models import Book, Chapter, Verse, Volume, init_db


class ScriptureAPI:
    def __init__(self, db_path):
        cherrypy.log(f"Checking for database at {db_path}")

        path = os.path.abspath(db_path)

        if not os.path.exists(path):
            raise RuntimeError(f"Database not found at {path}")

        self._db = init_db(path)
        self._rgx = re.compile(r"(?P<book>(?:([0-9]+\s)?)[A-Za-z ]+) (?P<verse>[0-9][0-9:\-,]+)")

        tmpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "templates")
        self._env = Environment(loader=FileSystemLoader(tmpl_dir))

        self._config = yaml.safe_load(open(os.environ["APP_CONFIG"]))

    def _render_template(self, tmpl_name, params={}):
        tmpl = self._env.get_template(tmpl_name)
        return tmpl.render(**params)

    @cherrypy.tools.json_out()
    @cherrypy.expose
    def index(self):
        return {
            "endpoints": {
                "query": {
                    "params": {
                        "text": "string",
                        "fmt": "string"
                    },
                    "returns" : [
                       {"type": "json",
                        "schema": {
                            "text": "string",
                            "response_type": "string"
                        }},
                        {"type": "html",
                         "schema": "string"
                        }
                    ]
                }
            }
        }

    @cherrypy.expose("ref")
    def query(self, text=None, fmt="slack", **kwargs):
        """ Query for scripture text

        Parameters
        ----------

        text : str
          Query text

        fmt : str, optional
          Format for response, defaults to 'slack'

        token : str
          Validation token for service

        Returns
        -------
        str
          Response body, either HTML text or string encoded JSON object
        """
        has_token = "token" in kwargs and kwargs["token"] in self._config["valid_tokens"]

        if fmt == "slack" and not has_token:
            return self.__json_fail__(400, "Missing valid token!")

        if not text:
            return self.__json_fail__(400, "Missing parameter(s): text")

        text = text.replace("+", " ")

        cherrypy.log(f"Got query string [{text}]")

        match = re.match(self._rgx, text)

        if not match:
            return self.__json_fail__(404, f"Could not find [{text}]")

        book = match.group("book")
        verses = match.group("verse")

        chapter = int(verses.split(":")[0])

        # Split on commas
        verses_list = verses.split(":")[1].split(",")

        verses_full = []

        # Parse ranges
        for item in verses_list:
            verse_range = tuple(map(int, item.split("-")))
            verses_full += [i for i in range(verse_range[0], verse_range[-1] + 1)]

        cherrypy.log(f"Identified book as [{book}]")
        cherrypy.log(f"Identified chapter as [{chapter}]")
        cherrypy.log(f"Identified verse(s) as [{verses_full}]")

        # Query book
        session = self._db()
        book_q = session.query(Book).filter((Book.book_title == book) |
                                            (Book.book_short_title == book) |
                                            (Book.book_long_title == book)).all()

        if not book_q:
            cherrypy.log("Failed to find book!")
            return self.__json_fail__(404, f"Could not find book [{book}]")

        if len(book_q) > 1:
            cherrypy.log("Too many matches!")
            cherrypy.log(f"Found: [book_q]")

            return self.__json_fail__(500, f"Too many matches, found [{book_q}]")

        cherrypy.log(f"Found book: [{book_q[0]}]")

        # Query chapters
        chapter_q = session.query(Chapter).filter((Chapter.book_id == book_q[0].id) &
                                                  (Chapter.chapter_number == chapter)).all()

        if not chapter_q:
            cherrypy.log("Failed to find chapter!")
            return self.__json_fail__(404, f"Could not find chapter [{chapter}]")

        cherrypy.log(f"Found chapter: [{chapter_q[0]}]")

        # Query verses
        verses_q = session.query(Verse).filter(and_((Verse.chapter_id == chapter_q[0].id),
                                                    or_(Verse.verse_number == v for v in verses_full)))

        if not verses_q:
            cherrypy.log("Failed to find verse(s)!")
            return self.__json_fail__(404, f"Could not find verse(s) [{verses_full}]")

        cherrypy.log(f"Found verses: [{list(v.verse_number for v in verses_q)}]")

        session.close()

        # Return formatted verses
        if fmt == "html":
            return self._render_template("verse.html", params={
                "book": book_q[0],
                "chapter": chapter_q[0],
                "verses": verses_q
            }).encode("utf-8")

        cherrypy.response.headers["Content-Type"] = "application/json"

        if fmt == "json":
            return json.dumps({
                "status": 200,
                "response": verses_full
            }).encode("utf-8")

        if fmt == "slack":
            return json.dumps({
                "text": self._render_template("slack.txt", params={
                    "book": book_q[0],
                    "chapter": chapter_q[0],
                    "verses": verses_q
                }),
                "response_type": "in_channel"
            }).encode("utf-8")

        return self.__json_fail__(400, f"Invalid format [{fmt}]")

    def __json_fail__(self, status, msg):
        """ Return JSON failure message

        Sets the response status and headers for a JSON error response, and
        builds a payload with the given status and message.

        Parameters
        ----------
        status : int
          HTTP response code

        msg : str
          Reason for error response

        Returns
        -------
        str
          Response body as a string representation of a JSON object
        """
        cherrypy.response.status = status
        cherrypy.response.headers["Content-Type"] = "application/json"

        return json.dumps({
            "status": status,
            "reason": msg
        }).encode("utf-8")
