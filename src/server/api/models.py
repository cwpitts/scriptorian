from sqlalchemy import Column, ForeignKey, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()


def init_db(path):
    engine = create_engine(f"sqlite:///{path}")
    return sessionmaker(bind=engine)

class Chapter(Base):
    """ Chapter model

    sqlite> .schema chapters
    CREATE TABLE chapters (
	id INTEGER PRIMARY KEY,
	book_id INTEGER REFERENCES books(id) ON DELETE CASCADE,
	chapter_number INTEGER
    );
    """
    __tablename__ = "chapters"

    id = Column(Integer, primary_key=True)
    book_id = Column(Integer, ForeignKey("books.id"))
    chapter_number = Column(Integer)

    def __repr__(self):
        return f"{self.chapter_number}"

    def __str__(self):
        return f"{self.chapter_number}"

class Volume(Base):
    """ Volume model

    sqlite> .schema volumes
    CREATE TABLE volumes (
	id INTEGER PRIMARY KEY,
	volume_title TEXT,
	volume_long_title TEXT,
	volume_subtitle TEXT,
	volume_short_title TEXT,
	volume_lds_url TEXT
    );
    """
    __tablename__ = "volumes"
    id = Column(Integer, primary_key=True)
    volume_title = Column(String)
    volume_long_title = Column(String)
    volume_subtitle = Column(String)
    volumme_short_title = Column(String)
    volume_lds_url = Column(String)

    def __repr__(self):
        return f"{self.volume_long_title}"

    def __str__(self):
        return f"{self.volume_long_title}"

class Verse(Base):
    """ Verse model

    sqlite> .schema verses
    CREATE TABLE verses (
	id INTEGER PRIMARY KEY,
	chapter_id INTEGER REFERENCES chapters(id) ON DELETE CASCADE,
	verse_number INTEGER,
	scripture_text TEXT
    );
    """
    __tablename__ = "verses"
    id = Column(Integer, primary_key=True)
    chapter_id = Column(Integer, ForeignKey("chapter.id"))
    verse_number = Column(Integer)
    scripture_text = Column(String)

    def __repr__(self):
        return f"{self.verse_number} {self.scripture_text}"

    def __str__(self):
        return f"{self.verse_number} {self.scripture_text}"

class Book(Base):
    """ Book model

    sqlite> .schema books
    CREATE TABLE books (
	id INTEGER PRIMARY KEY,
	volume_id INTEGER REFERENCES volumes(id) ON DELETE CASCADE,
	book_title TEXT,
	book_long_title TEXT,
	book_subtitle TEXT,
	book_short_title TEXT,
	book_lds_url TEXT
    );
    """
    __tablename__ = "books"
    id = Column(Integer, primary_key=True)
    volume_id = Column(Integer, ForeignKey("volumes.id"))
    book_title = Column(String)
    book_long_title = Column(String)
    book_subtitle = Column(String)
    book_short_title = Column(String)
    book_lds_url = Column(String)

    def __repr__(self):
        return f"{self.book_long_title} {self.book_subtitle}".strip()
